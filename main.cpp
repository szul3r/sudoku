// Wykonał Radosław Zawisza Szylkowski //

#include <iostream>


using namespace std;

bool rozwiazanie(int,int);
bool spr_wprowadzenia(int,int,int);
bool nastepne_pole(int,int);

/* zerami wypełniamy w tablicy puste pola */

int sudoku[9][9] = {{5,3,0,0,7,0,0,0,0},
                    {6,0,0,1,9,5,0,0,0},
                    {0,9,8,0,0,0,0,6,0},
                    {8,0,0,0,6,0,0,0,3},
                    {4,0,0,8,0,3,0,0,1},
                    {7,0,0,0,2,0,0,0,6},
                    {0,6,0,0,0,0,2,8,0},
                    {0,0,0,4,1,9,0,0,5},
                    {0,0,0,0,8,0,0,7,9}};
/*
  * tablica testowa 1
  {5,3,0,0,7,0,0,0,0},
  {6,0,0,1,9,5,0,0,0},
  {0,9,8,0,0,0,0,6,0},
  {8,0,0,0,6,0,0,0,3},
  {4,0,0,8,0,3,0,0,1},
  {7,0,0,0,2,0,0,0,6},
  {0,6,0,0,0,0,2,8,0},
  {0,0,0,4,1,9,0,0,5},
  {0,0,0,0,8,0,0,7,9}
*/
/*
 * tablica testowa 2
  {2,0,0,6,0,7,5,0,0},
  {0,0,0,0,0,0,0,9,6},
  {6,0,7,0,0,1,3,0,0},
  {0,5,0,7,3,2,0,0,0},
  {0,7,0,0,0,0,0,2,0},
  {0,0,0,1,8,9,0,7,0},
  {0,0,3,5,0,0,6,0,4},
  {8,4,0,0,0,0,0,0,0},
  {0,0,5,2,0,6,0,0,8}
*/

/*
 * tablica testowa 3
  {0,1,0,0,5,6,2,7,0},
  {0,0,0,0,8,0,0,0,9},
  {0,7,8,0,0,3,6,0,5},
  {0,0,0,0,0,4,5,0,1},
  {8,5,2,0,0,0,7,3,4},
  {6,0,1,7,0,0,0,0,0},
  {1,0,6,4,0,0,9,5,0},
  {3,0,0,0,6,0,0,0,0},
  {0,2,7,3,9,0,0,8,0}
*/
int obecne[9][9];

int main()
{
    for(int i=0;i<9;i++)
    {
        for(int j=0;j<9;j++)
        {
            obecne[i][j] = sudoku[i][j];  //tworzenie kopii do porównania
        }
    }
    if (rozwiazanie(0,0))
    {
        for(int i=0;i<9;i++)
        {
            if((i%3==0) && (i!=0)){cout << "------------\n";}
            for(int j=0;j<9;j++)
            {
                if(j%3==0){cout<< "|";}
                cout<<obecne[i][j];
            }
            cout<<endl;
        }
    }
    else
    {
        cout<<"brak rozwiazn dla takiego ukladu liczb\n";
    }
    return 0;
}




bool spr_wprowadzenia(int x,int y,int z)
{
    for(int i=0;i<9;i++)
    {
        if ((z == obecne[x][i]) || (z == obecne[i][y]) ||
                (z == obecne[x/3*3+i%3][y/3*3+i/3]))            // sprawdza czy w poziomie pionie i kwadracie 3x3
        {                                                       // występuje liczba 'z' przekazana z funkcji rozwiazanie()
            return false;                                       // jako parametr który pochodzi z pętli for. Jeśli występuje
        }                                                       // zwraca false i przechodzi do kolejnej iteracji pętli w
    }                                                           // funkcji roziwazanie()
    return true;
}

bool nastepne_pole(int x,int y)
{
    if ((x==8) && (y==8))
    {
        return true;
    }
    else if (x == 8)
    {
        return rozwiazanie(0,y+1);
    }
    else
    {
        return rozwiazanie(x+1,y);
    }
}

bool rozwiazanie(int x,int y)
{
    if (sudoku[x][y] == 0)  //sprawdzenie czy pole posiada rozwiązanie ( 0 oznacza brak rozwiazania)
    {
        for(int i=1;i<=9;i++)
        {
            if (spr_wprowadzenia(x,y,i)) // Jeśli true to wpisuje do kopii sudoku wartość i
            {
                obecne[x][y] = i;
                if (nastepne_pole(x,y)) // Przejście do następnego pola
                {
                    return true;
                }
            }
        }
        obecne[x][y] = 0;
        return false;
    }
    return nastepne_pole(x,y);
}
